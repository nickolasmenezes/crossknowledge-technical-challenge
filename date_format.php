<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>CrossKnowledge - Code challenge</title>
</head>
<body>
  <script>
    // This will create elements for testing, DO NOT change this code
    (() => {
      const MS_PER_MINUTE = 60000
      const NOW = new Date()
      let minutes = [0, 1, 30, 60, 6 * 60, 23 * 60, 24 * 60]
      let dates = []

      minutes.forEach((i) => dates.push(new Date(NOW - i * MS_PER_MINUTE)))

      dates.forEach((item) => {
        let el = document.createElement("div")
        el.innerHTML = "Started "

        let dt = document.createElement('span')
        dt.className = 'js-date-format'
        dt.innerHTML = item.toISOString()
        el.appendChild(dt)
        document.body.appendChild(el)
      })

      let intervalId = window.setInterval(function(){
        replaceByDifferenceTimeFromInitialDate(dates);
      }, 1000);

    })();

    function replaceByDifferenceTimeFromInitialDate(dates_js){
      let current_date = new Date();

      for(var i=0; i<dates_js.length; i++){
        let html_element = document.getElementsByClassName("js-date-format")[i];
        let aux_date = new Date(dates_js[i]);
        let difference_between_dates_in_seconds = (current_date - aux_date) / 1000;
        html_element.innerHTML = getHoursMinutesFromSecondsFormatted(difference_between_dates_in_seconds);
      }

    }

    function getHoursMinutesFromSecondsFormatted(seconds){
      let num_hours = Math.floor(seconds / 3600);
      let num_minutes = Math.floor(seconds % 3600 / 60);
      let num_seconds = Math.floor(seconds % 3600 % 60);

      let hasHours = num_hours > 0;
      let hasMinutes = num_minutes > 0;
      let hasOnlySeconds = !hasHours && !hasMinutes && num_seconds > 0;

      let text_hours = hasHours ? num_hours + (num_hours == 1 ? " hour ago " : " hours ago") : "";
      let text_minutes = !hasHours && hasMinutes ? num_minutes + (num_minutes == 1 ? " minute ago " : " minutes ago") : "";
      let text_seconds = hasOnlySeconds ? num_seconds + (num_seconds == 1 ? " second ago" : " seconds ago") : "";

      return text_hours + text_minutes + text_seconds;
    }

  </script>
</body>
</html>
