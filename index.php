<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Nickolas Menezes da Silva - CrossKnowledge Challenge</title>
        <!-- Favicon-->
        <link rel="icon" type="image/x-icon" href="resources/assets/img/logo.png" />
        <!-- Font Awesome icons (free version)-->
        <script src="https://use.fontawesome.com/releases/v5.15.1/js/all.js" crossorigin="anonymous"></script>
        <!-- Google fonts-->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="resources/css/styles.css" rel="stylesheet" />
    </head>
    <body id="page-top">
        <!-- Navigation-->
        <nav class="navbar navbar-expand-lg bg-secondary text-uppercase fixed-top" id="mainNav">
            <div class="container">
                <a class="navbar-brand js-scroll-trigger" href="#page-top">CrossKnowledge Challenge</a>
                <button class="navbar-toggler navbar-toggler-right text-uppercase font-weight-bold bg-primary text-white rounded" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    Menu
                    <i class="fas fa-bars"></i>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item mx-0 mx-lg-1"><a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#cache_function">1. Cache function</a></li>
                        <li class="nav-item mx-0 mx-lg-1"><a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#date_format">2. Date formatting</a></li>
                        <li class="nav-item mx-0 mx-lg-1"><a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#apply_style">3. Apply style</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- Masthead-->
        <header class="masthead bg-primary text-white text-center">
            <div class="container d-flex align-items-center flex-column">
                <!-- Masthead Avatar Image-->
                <img class="mb-5 rounded-circle" src="resources/assets/img/perfil.png" alt="" />
                <!-- Masthead Heading-->
                <h1 class="masthead-heading text-uppercase mb-0">Nickolas Menezes da Silva</h1>
                <!-- Icon Divider-->
                <div class="divider-custom divider-light">
                    <div class="divider-custom-line"></div>
                    <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
                    <div class="divider-custom-line"></div>
                </div>
                <!-- Masthead Subheading-->
                <p class="masthead-subheading font-weight-light mb-0"> Candidate to Full Stack Engineer Vacancy </p>
            </div>
        </header>
        <!-- cache_function Section-->
        <section class="page-section cache_function" id="cache_function">
            <div class="container">
                <!-- cache_function Section Heading-->
                <h2 class="page-section-heading text-center text-uppercase text-secondary mb-0">1. Cache function</h2>
                <!-- Icon Divider-->
                <div class="divider-custom">
                    <div class="divider-custom-line"></div>
                    <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
                    <div class="divider-custom-line"></div>
                </div>
                <h4> It was used: </h4>
                <ul class="list-group">
                  <li class="list-group-item">Composer to manage the dependencies <a target="_blank" href="https://getcomposer.org/"> (https://getcomposer.org/) </a> </li>
                  <li class="list-group-item">Predis Client <a target="_blank" href="https://github.com/predis/predis"> (https://github.com/predis/predis) </a> </li>
                  <li class="list-group-item">Official Redis Docker to manage the cache <a target="_blank" href="https://hub.docker.com/_/redis"> (https://hub.docker.com/_/redis) </a></li>
                  <li class="list-group-item">REQ | RES to test the requests and provide sample data  <a target="_blank" href="https://reqres.in/"> (https://reqres.in/) </a></li>
                </ul>
                <br />

                <div class="card" style="width: 100%;">
                  <div class="card-body">
                    <h5 class="card-title">Modified file (request.php) </h5>
                        <?php highlight_file('src/request.php'); ?>
                  </div>
                </div>
                <br />
                <div class="row">
                  <div class="col-md-7">
                    <div class="card" style="width: 100%;">
                      <div class="card-body">
                        <h5 class="card-title">Use case</h5>
                        <?php highlight_file('src/usecaserequest.php'); ?>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-5">
                    <div class="card" style="width: 100%;">
                      <div class="card-body">
                        <h5 class="card-title">Printed response</h5>
                        <iframe frameBorder="0" style="width: 100%; min-height:800px" src="src/usecaserequest.php"> </iframe>
                      </div>
                    </div>
                  </div>
                </div>

            </div>
        </section>
        <!-- date_format Section-->
        <section class="page-section bg-primary text-white mb-0" id="date_format">
            <div class="container">
                <!-- date_format Section Heading-->
                <h2 class="page-section-heading text-center text-uppercase text-white">2. Date formatting</h2>
                <!-- Icon Divider-->
                <div class="divider-custom divider-light">
                    <div class="divider-custom-line"></div>
                    <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
                    <div class="divider-custom-line"></div>
                </div>
                <div class="row">
                    <div class="col-lg-12 ml-auto">
                      <div class="card" style="width: 100%;">
                        <div class="card-body">
                          <h5 class="card-title" style="color: #2c3e50 ">Code Running </h5>
                            <iframe frameBorder="0" src="date_format.php"> </iframe>
                        </div>
                      </div>
                    </div>
                </div><br />
                <!-- date_format Section Content-->
                <div class="row">
                    <div class="col-lg-12 ml-auto">
                      <div class="card" style="width: 100%;">
                        <div class="card-body">
                          <h5 class="card-title" style="color: #2c3e50 ">Source code (date_format.php) </h5>
                              <?php show_source('date_format.php'); ?>
                        </div>
                      </div>
                    </div>
                </div>

            </div>
        </section>
        <!-- apply_style Section-->
        <section class="page-section" id="apply_style">
            <div class="container">
                <!-- apply_style Section Heading-->
                <h2 class="page-section-heading text-center text-uppercase text-secondary mb-0">3. Apply style</h2>
                <!-- Icon Divider-->
                <div class="divider-custom">
                    <div class="divider-custom-line"></div>
                    <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
                    <div class="divider-custom-line"></div>
                </div>
                <div class="row">
                    <div class="col-lg-12 ml-auto">
                      <div class="card" style="width: 100%;">
                        <div class="card-body">
                          <h5 class="card-title" style="color: #2c3e50 ">Code Running </h5>
                            <iframe frameBorder="0" style="width:100%;height: 800px" src="component.php"> </iframe>
                        </div>
                      </div>
                    </div>
                </div><br />
                <!-- apply_style Section Form-->
                <div class="row">
                    <div class="col-lg-12 mx-auto">
                      <!-- date_format Section Content-->
                      <div class="row">
                          <div class="col-lg-12 ml-auto">
                            <div class="card" style="width: 100%;">
                              <div class="card-body">
                                <h5 class="card-title">Source code (component.php) </h5>
                                    <?php show_source('component.php'); ?>
                              </div>
                            </div>
                          </div>
                      </div><br />
                      <div class="row">
                          <div class="col-lg-12 ml-auto">
                            <div class="card" style="width: 100%;">
                              <div class="card-body">
                                <h5 class="card-title">Source code (resources/component-style.css) </h5>
                                    <?php show_source('resources/component-style.css'); ?>
                              </div>
                            </div>
                          </div>
                      </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Copyright Section-->
        <div class="copyright py-4 text-center text-white">
            <div class="container"><small>Copyright © Nickolas Menezes da Silva - 2021 <br /> <small> Theme based on <a href="http://startbootstrap.com">Start Bootstrap </small></a> </small></div>
        </div>
        <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes)-->
        <div class="scroll-to-top d-lg-none position-fixed">
            <a class="js-scroll-trigger d-block text-center text-white rounded" href="#page-top"><i class="fa fa-chevron-up"></i></a>
        </div>

        <!-- Bootstrap core JS-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Third party plugin JS-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
        <!-- Core theme JS-->
        <script src="resources/js/scripts.js"></script>
    </body>
</html>
