<?php

  define('__ROOT__', dirname(dirname(__FILE__)));
  require_once(__ROOT__.'/vendor/autoload.php');
  require_once('request.php');
  header('Content-type: text/javascript');

  Predis\Autoloader::register();
  $redis = new Predis\Client([
      'scheme' => 'tcp',
      'host'   => '127.0.0.1',
      'port'   => 6379,
  ]);

  $url = "https://reqres.in/api/users";
  $parameters = array('page'=>'2');
  $simpleJsonRequest = new SimpleJsonRequest($redis);
  $response = $simpleJsonRequest->get($url, $parameters);
  echo json_encode($response, JSON_PRETTY_PRINT);

?>
