<?php

require_once(__ROOT__.'/vendor/autoload.php');
Predis\Autoloader::register();

class SimpleJsonRequest
{
    public $redis;

    public function __construct($redis) {
        $this->redis = $redis;
    }

    private function makeRequest(string $method, string $url, array $parameters = null, array $data = null)
    {
        $opts = [
            'http' => [
                'method'  => $method,
                'header'  => 'Content-type: application/json',
                'content' => $data ? json_encode($data) : null
            ]
        ];

        $url .= ($parameters ? '?' . http_build_query($parameters) : '');

        $key = $this->generateCacheKey($opts, $url);
        if($this->checkIfKeyExistsInCache($key)){
            $responseFromCache = $this->getResponseFromCache($key);
            return $responseFromCache;
        }else{
            $response = file_get_contents($url, false, stream_context_create($opts));
            $this->doCache($key, $response, 3600);
            return $response;
        }
    }

    public function get(string $url, array $parameters = null)
    {
        return json_decode($this->makeRequest('GET', $url, $parameters));
    }

    public function post(string $url, array $parameters = null, array $data)
    {
        return json_decode($this->makeRequest('POST', $url, $parameters, $data));
    }

    public function put(string $url, array $parameters = null, array $data)
    {
        return json_decode($this->makeRequest('PUT', $url, $parameters, $data));
    }

    public function patch(string $url, array $parameters = null, array $data)
    {
        return json_decode($this->makeRequest('PATCH', $url, $parameters, $data));
    }

    public function delete(string $url, array $parameters = null, array $data = null)
    {
        return json_decode($this->makeRequest('DELETE', $url, $parameters, $data));
    }

    public function getResponseFromCache(string $key)
    {
        return $this->redis->get($key);
    }

    public function doCache(string $key, string $response, $timeToLiveInMilliseconds = 3600)
    {
        $this->redis->setex($key, $timeToLiveInMilliseconds, $response);
    }

    public function checkIfKeyExistsInCache(string $key)
    {
        return $this->redis->exists($key);
    }

    public function generateCacheKey(array $opts, string $url)
    {
        $allRequestedParameters = "{url : '". $url ."', parameters : ". json_encode($opts). "}";
        return md5($allRequestedParameters);
    }


}
