Technical challenge performed by Nickolas Menezes da Silva to Full stack vacancy at CrossKnowledge

**Instructions**

1- Redis official docker from https://hub.docker.com/_/redis
or just put in your terminal


`docker pull redis`

`sudo docker run -d -p 6379:6379 -i -t redis`


2- Clone the project in  your http/php server: 

`git clone https://gitlab.com/nickolasmenezes/crossknowledge-technical-challenge.git`

3- Access in your browser 

[http://localhost/CrossKnowledge/index.php](http://localhost/CrossKnowledge/index.php)



Thank you!

Nickolas Menezes da Silva




